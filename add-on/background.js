const mullvadWireGuardProxy = {
	proxyType: 'manual',
	socks: '10.64.0.1:1080',
	proxyDNS: true
}

const setIconToShieldsDown = () => {
	window.browser.browserAction.setIcon({
		path: {
			96: 'icons/icons8-warning-shield-100.png'
		}
	})
}

const setIconToShieldsUp = () => {
	window.browser.browserAction.setIcon({
		path: {
			96: 'icons/icons8-protect-shield-100.png'
		}
	})
}

const enableMullvadWireGuardProxy = () => {
	window.browser.proxy.settings.set({
		value: mullvadWireGuardProxy
	})
}

const disableMullvadWireGuardProxy = () => {
	window.browser.proxy.settings.set({
		value: {
			proxyType: 'none'
		}
	})
}

const toggleMullvadWireGuardProxy = () => {
	window.browser.proxy.settings.get({}).then(proxySettings => {
		if (proxySettings.value.proxyType === 'manual') {
			disableMullvadWireGuardProxy()
			setIconToShieldsDown()
		} else {
			enableMullvadWireGuardProxy()
			setIconToShieldsUp()
		}
	})
}

const isProxySetToMullvadWireGuard = () => {
	window.browser.proxy.settings.get({}).then(proxySettings => {
		if (proxySettings.value.proxyType === mullvadWireGuardProxy.proxyType
			&& proxySettings.value.socks === mullvadWireGuardProxy.socks
			&& proxySettings.value.proxyDNS === mullvadWireGuardProxy.proxyDNS) {
			setIconToShieldsUp()
		} else {
			setIconToShieldsDown()
		}
	})
}

// init
isProxySetToMullvadWireGuard()
window.browser.browserAction.onClicked.addListener(toggleMullvadWireGuardProxy)
