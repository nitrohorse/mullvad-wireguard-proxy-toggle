# Mullvad WireGuard Proxy Toggle

[![License](https://img.shields.io/badge/license-GPLv3-ff69b4.svg)](https://gitlab.com/nitrohorse/mullvad-wireguard-proxy-toggle/blob/master/LICENSE)
[![Standard Style](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)
[![Downloads](https://img.shields.io/amo/users/mullvad-wireguard-proxy-toggle.svg)](https://addons.mozilla.org/en-US/firefox/addon/mullvad-wireguard-proxy-toggle/)
[![Say Thanks!](https://img.shields.io/badge/Say%20Thanks-!-1EAEDB.svg)](https://saythanks.io/to/nitrohorse)

Firefox extension for toggling on/off [Mullvad](https://mullvad.net)'s [SOCKS5 WireGuard proxy](https://mullvad.net/en/guides/socks5-proxy/#wireguard-socks5). Forked with gratefulness from [xapax](https://github.com/xapax/proxy-toggle).

## Download
* https://addons.mozilla.org/en-US/firefox/addon/mullvad-wireguard-proxy-toggle

## Develop Locally
* Clone the repo
* Install tools:
	* [Node.js](https://nodejs.org)
* Install dependencies: 
	* `npm i`
* Run add-on in isolated Firefox instance using [web-ext](https://developer.mozilla.org/en-US/Add-ons/WebExtensions/Getting_started_with_web-ext) (open the [Browser Toolbox](https://developer.mozilla.org/en-US/docs/Tools/Browser_Toolbox) for console logging):
	* `npm start`
* Lint:
	* `npm run lint`
* Package for distribution:
	* `npm run bundle`
